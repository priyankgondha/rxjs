import { Component, OnInit } from '@angular/core';
import { concat, interval, map, take } from 'rxjs';
import { DesignUtilityService } from 'src/app/design-utility.service';

@Component({
  selector: 'app-concat',
  templateUrl: './concat.component.html',
  styleUrls: ['./concat.component.css']
})
export class ConcatComponent implements OnInit {

  constructor(private ds: DesignUtilityService) { }

  ngOnInit(): void {

    const sourceTech = interval(1000).pipe(map(v => 'Tech Video #' + (v + 1)), take(5));
    const sourceComedy = interval(1000).pipe(map(v => 'Comedy Video #' + (v + 1)), take(5));
    const sourceNews = interval(1000).pipe(map(v => 'News Video #' + (v + 1)), take(5));


    const FinalObs = concat(sourceTech, sourceComedy, sourceNews)
    FinalObs.subscribe(res => {
      console.log(res);
      this.ds.print(res, 'elContainer');
    })

  }

}

import { Component, OnInit } from '@angular/core';
import { filter, from, toArray } from 'rxjs';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {

  datArr = [
    { id: 1, name: 'Priyank', gender: 'Male' },
    { id: 2, name: 'Kristal', gender: 'Female' },
    { id: 3, name: 'Krushik', gender: 'Male' },
    { id: 4, name: 'Megha', gender: 'Female' },
    { id: 5, name: 'Utam', gender: 'Male' },
  ]
  databylenght;
  databygender;
  databynthitem;

  constructor() { }

  ngOnInit(): void {

    const source = from(this.datArr);

    // Ex-1 Filter by Length
    source.pipe(
      filter(member => member.name.length > 5),
      toArray())
      .subscribe(res => {
        console.log(res);
        this.databylenght = res;
      })


    // Ex-2 Filter by Gender
    source.pipe(
      filter(member => member.gender == 'Female'),
      toArray())
      .subscribe(res => {
        console.log(res);
        this.databygender = res;
      })


    // Ex-3 Filter by nth Item
    source.pipe(
      filter(member => member.id == 3),
      toArray())
      .subscribe(res => {
        console.log(res);
        this.databynthitem = res;
      })
  }

}

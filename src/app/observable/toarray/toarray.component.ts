import { Component, OnInit } from '@angular/core';
import { interval, Subscription, toArray, take, from, of } from 'rxjs';

@Component({
  selector: 'app-toarray',
  templateUrl: './toarray.component.html',
  styleUrls: ['./toarray.component.css']
})
export class ToarrayComponent implements OnInit {

  users = [
    { name: 'Priyank', skill: 'Angular' },
    { name: 'Karan', skill: 'MBBS' },
    { name: 'Krushik', skill: 'Chemist' }
  ]

  constructor() { }

  sourceSub: Subscription;
  ngOnInit(): void {
    const source = interval(1000);

    // Ex-1
    this.sourceSub = source.pipe(take(5), toArray()).subscribe(res => {
      console.log(res);
    })

    // Ex-2
    const source2 = from(this.users);
    source2.pipe(toArray()).subscribe(res => {
      console.log(res)
    })

    // Ex-3
    const source3 = of('priyank', 'krutik');
    source3.pipe(toArray()).subscribe(res => {
      console.log(res)
    })
  }

}

import { Component, OnInit } from '@angular/core';
import { ResolveEnd } from '@angular/router';
import { from, of } from 'rxjs';
import { DesignUtilityService } from 'src/app/design-utility.service';

@Component({
  selector: 'app-of-from',
  templateUrl: './of-from.component.html',
  styleUrls: ['./of-from.component.css']
})
export class OfFromComponent implements OnInit {

  constructor(private ds: DesignUtilityService) { }
  objmsg;

  ngOnInit(): void {

    // OF example
    const Obs1 = of('Anup', 'Priyank', 'Karan');
    Obs1.subscribe(res => {
      console.log(res);
      this.ds.print(res, 'elContainer')
    })


    const Obs2 = of({ a: 'Krushik', b: 'Priyank', c: 'Karan' });
    Obs2.subscribe(res => {
      this.objmsg = res
      console.log('objmsg=>', res);
    })

    //FROM example - Array
    const Obs3 = from(['Utam', 'Priyank', 'Savan']);
    Obs3.subscribe(res => {
      console.log(res);
      this.ds.print(res, 'elContainer3')
    })

    //From example - Promise
    const promise = new Promise((resolve) => {
      setTimeout(() => {
        resolve('Promise Resolved')
      }, 2000);
    })
    const Obs4 = from(promise);
    Obs4.subscribe(res => {
      console.log('from Promise', res);
      this.ds.print(res, 'elContainer4')
    })

    //From example- String
    const Obs5 = from('Welcome');
    Obs5.subscribe(res => {
      console.log('from String=>', res);
      this.ds.print(res, 'elContainer5')
    })

  }

}

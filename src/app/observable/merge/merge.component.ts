import { Component, OnInit } from '@angular/core';
import { interval, map, merge, take } from 'rxjs';
import { DesignUtilityService } from 'src/app/design-utility.service';

@Component({
  selector: 'app-merge',
  templateUrl: './merge.component.html',
  styleUrls: ['./merge.component.css']
})
export class MergeComponent implements OnInit {

  constructor(private ds: DesignUtilityService) { }

  ngOnInit(): void {

    const sourceTech = interval(1000).pipe(map(v => 'Tech Video #' + (v + 1)), take(5));
    const sourceComedy = interval(3000).pipe(map(v => 'Comedy Video #' + (v + 1)), take(5));
    const sourceNews = interval(1500).pipe(map(v => 'News Video #' + (v + 1)), take(5));


    const FinalObs = merge(sourceTech, sourceComedy, sourceNews)
    FinalObs.subscribe(res => {
      console.log(res);
      this.ds.print(res, 'elContainer');
    })

  }

}

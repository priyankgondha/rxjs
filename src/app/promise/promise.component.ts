import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-promise',
  templateUrl: './promise.component.html',
  styleUrls: ['./promise.component.css']
})
export class PromiseComponent implements OnInit {

  constructor() { }

  promiseval: any;
  dell = {
    brand: 'DELL',
    harddisk: '2 Tb'
  }
  hp = {
    brand: 'HP',
    harddisk: '2 Tb'
  }

  dellavailable() {

    return true;

  }
  hpavailable() {

    return false;

  }

  ngOnInit(): void {


    let buylaptop = new Promise((resolve, reject) => {
      // resolve('promise is solved');
      // reject("Promise is rejected");
      if (this.dellavailable()) {
        return setTimeout(() => {
          resolve(this.dell);
        }, 2000)
      } else if (this.hpavailable()) {
        return setTimeout(() => {
          resolve(this.hp);
        }, 2000)
      } else {
        return reject("no laptop");
      }

    })
    buylaptop.then(res => {
      console.log("success code => ", res);
      this.promiseval = res;
    }).catch(res => {
      console.log("catch code=> ", res)
      this.promiseval = res;
    })
  }


}
